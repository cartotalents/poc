
const { makeSchema, objectType, stringArg, floatArg, asNexusMethod } = require('@nexus/schema')
const { GraphQLDate } = require('graphql-iso-date')
const { PrismaClient } = require('@prisma/client')
const { graphql } = require('graphql')
const path = require('path')
const express = require('express'),
      router = express.Router();

const discovery = require('../discovery');
const GQLDate = asNexusMethod(GraphQLDate, 'date')

const prisma = new PrismaClient()

//Codefirst types definition, which will generate ./schema.graphql via nexus
const User = objectType({
  name: 'User',
  definition(t) {
    t.int('id')
    t.string('name')
    t.string('email')
    t.float('geoLat')
    t.float('geoLon')
    t.string('instance_id', {nullable: true}) //No data in this field, as it's dynamically added only when retrieve by a distant server, via /api/remote
  },
})

//Queries and their resolver
const Query = objectType({
  name: 'Query',
  definition(t) {
    t.field('user', {
      type: 'User',
      args: {
        userId: stringArg({ nullable: false }),
      },
      resolve: (_, args) => {
        return prisma.user.findOne({
          where: { id: Number(args.userId) },
        })
      },
    })

    t.list.field('allUsers', {
      type: 'User',
      resolve: (_parent, _args, ctx) => {
        return prisma.user.findMany()
      },
    })
  },
})

//Mutations and their resolver
const Mutation = objectType({
  name: 'Mutation',
  definition(t) {
    t.field('signupUser', {
      type: 'User',
      args: {
        name: stringArg(),
        email: stringArg({ nullable: false }),
        geoLat: floatArg({ nullable: false }),
        geoLon: floatArg({ nullable: false }),
      },
      resolve: (_, { name, email, geoLat, geoLon }, ctx) => {
        return prisma.user.create({
          data: {
            name,
            email,
            geoLat,
            geoLon,
          },
        })
      },
    })

    t.field('deleteUser', {
      type: 'User',
      nullable: true,
      args: {
        userId: stringArg(),
      },
      resolve: (_, { userId }, ctx) => {
        return prisma.user.delete({
          where: { id: Number(userId) },
        })
      },
    })
  },
})

//Dynamic generation of the schema
const schema = makeSchema({
  types: [Query, Mutation, User, GQLDate],
  outputs: {
    typegen: path.join(process.cwd(), 'api', 'nexus-typegen.ts'),
    schema: path.join(process.cwd(), 'api', 'schema.graphql')
  },
})

/**
 * Queries the local database, with the defined resolvers (goes through prisma / postgre)
 * @param query Query object from the original GQL request
 * @param variables Variables object from the original GQL request
 * @returns GraphQL response, as a JSON. Ex:
 * {
 *   "data": {
 *     "allUsers": [
 *       {
 *         ...
 *       }
 *     ]
 *   }
 * }
 */
const queryLocalDatabase = async (query, variables) => {
  try {
    const response = await graphql(schema, query, {}, {}, variables)
    return response;
  }
  catch (e) {
    console.error(e);
    return null;
  }
}

/**
 * Queries the local database, and add the instance ID (process.env.INSTANCE_ID) in every entity
 * Used when a remote instance fetch this instance's data via /api/remote, 
 * so the remote instance can diffrentiate where data came from
 * 
 * @param query Query object from the original GQL request
 * @param variables Variables object from the original GQL request
 * 
 * @returns GraphQL response, as a JSON. Ex:
 * {
 *   "data": {
 *     "allUsers": [
 *       {
 *         ...
 *         instance_id: "a.poc.local"
 *       }
 *     ]
 *   }
 * }
 */
const queryLocalDatabaseAndAddInstanceID = async (query, variables) => {
  const response = await queryLocalDatabase(query, variables);
  const queries = Object.keys(response.data);
  //Add the instance_id on each object, to differentiate them visually on the map
  let alteredResponse = { data: {} };
  queries.forEach(queryKey => {
    alteredResponse.data[queryKey] = response.data[queryKey].map(item => ({ ...item, id: `${process.env.INSTANCE_ID}.${item.id}`, instance_id: process.env.INSTANCE_ID}))
  });
  return alteredResponse
}
/**
 * Builds a promise that resolve on fetch .json() function, to use in conjuction with Promise.all()
 * Used to query remote data.
 * 
 * @param url Instance url with protocol - ex: http://a.poc.local
 * @param query Query object from the original GQL request
 * @param variables Variables object from the original GQL request
 * @returns GQLresult
 */
const buildFetchPromise = async (url, query, variables) => {
  return new Promise((resolve, reject) => {
    fetch(`${url}/api/remote`, { method: 'post', body: JSON.stringify({ query, variables }), headers: { 'Content-Type': 'application/json' } }).then(result => {
      resolve(result.json());
    })
  })
}
/**
 * Merge local and distant results into a single object, directly exploitable by the client
 * 
 * @param localResults Results from local database
 * @param distantResults Array of results from remote instances
 */
const mergeLocalAndDistantResults = (localResults: any, distantResults: [any]) => {
  //If there isn't any data, quit now
  if (!localResults || !localResults.data)
    return null

  //Get all queries from result object
  const queries = Object.keys(localResults.data);
  //Then merge all these in a new object, clone of localResults
  let mergedResponse = { data: {} };
  queries.forEach(queryKey => {
    const flattenedDistantResults = distantResults.reduce((acc, result) => acc.concat(result.data[queryKey]), [])
    // console.log(`${queryKey} - local : ${JSON.stringify(localResults.data[queryKey], null, 2)} ; distant : ${JSON.stringify(flattenedDistantResults, null, 2)} `)
    mergedResponse.data[queryKey] = localResults.data[queryKey].concat(flattenedDistantResults)
  });
  return mergedResponse
}

/**
 * Queries all followed remote instances database, by passing user's original query/variables to the followed instances
 * Uses a fetch on remote's /api/remote endpoints.
 * Based on the discovery module, which uses a static file listing the instances and 
 * describing the relationships between the instances for now (instances.json)
 *
 * @param query Query object from the original GQL request
 * @param variables Variables object from the original GQL request
 *
 * @returns Array of GraphQL responses. Ex:
 * [{
 *   "data": {
 *     "allUsers": [
 *       {
 *         ...
 *         instance_id: "a.poc.local"
 *       }
 *     ]
 *   }
 * }]
 */
const queryDistantDatabases = async (query, variables) => {
  try {
    const instances = discovery.api.getInstanceFollowed();
    console.log(`Query instances : ${JSON.stringify(instances, null, 2)}`)
    const fetchPromises = instances.map(instance => buildFetchPromise(instance.url, query, variables))
    const results = await Promise.all(fetchPromises);
    return results;
  }
  catch (e) {
    console.error(e);
    return null;
  }
}
/**
 * Base /api route, locally used
 * @param req Express req object
 * @param res Express res object
 */
router.post("/", async (req, res) => {
  console.log(`Query GQL : ${JSON.stringify(req.body, null, 2)}`)
  const response = await queryLocalDatabase(req.body.query, req.body.variables)
  console.log(`Response local : ${JSON.stringify(response, null, 2)}`)
  //Query distant content only if its a global retrieval query
  //TODO: find a better way to retrieve remote data conditionnaly
  if(req.body.query.match(/(all)/)) {
    const distantResponses = await queryDistantDatabases(req.body.query, req.body.variables)
    console.log(`Response of other servers : ${JSON.stringify(distantResponses, null, 2)}`)
  
    const mergedResponse = mergeLocalAndDistantResults(response, distantResponses)
    console.log(`Merge of response : ${JSON.stringify(mergedResponse, null, 2)}`)
  
    return mergedResponse ? res.json(mergedResponse) : res.status(500).end()
  }
  else
    return response ? res.json(response) : res.status(500).end()
});
/**
 * /api/remote route, used by remote instances to fetch data
 * @param req Express req object
 * @param res Express res object
 */
router.post("/remote", async (req, res) => {
  const response = await queryLocalDatabaseAndAddInstanceID(req.body.query, req.body.variables)
  console.log(`Fetch from other service : ${JSON.stringify(response, null, 2)}`)

  return response ? res.json(response) : res.status(500).end()
});

module.exports = router
