import React, { useState } from 'react'
import Layout from '../components/Layout'
import Router from 'next/router'

//The JSON object (can't retrieve the API for now)
import instances from '../discovery/instances.json'
//Interface import
import {Instance} from '../discovery'
import HelpBlock from '../components/HelpBlock'

interface InstanceRowProps {
  instance: Instance
}
function InstanceRow({instance}) {
  return (
    <a href={instance.url} className="instance-row">
      <div>
        <h2>{instance.name}</h2>
        <p>Location : {instance.url}</p>
        <p>ID : {instance.id}</p>
      </div>
      <div className="status">
        <p className={instance.isFollowed ? 'followed' : 'not-followed'}>Followed : {instance.isFollowed ? 'yes' : 'no'}</p>
      </div>

      <style jsx>{`
        .instance-row {
          display: flex;
          text-decoration: none;
          color: initial;
          align-items: center;
          justify-content: space-between;
          background: white;
          transition: box-shadow 0.1s ease-in;
          padding: 0px 24px 4px;
          box-shadow: 0px 1px 1px #aaa;
        }

        .instance-row:hover {
          box-shadow: 0px 3px 3px #aaa;
        }

        .instance-row + .instance-row {
          margin-top: 2rem;
        }
        .instance-row .status {
          font-weight: bold;
        }
        .instance-row .followed {
          color: green;
        }
        .instance-row .not-followed {
          color: red;
        }
      `}</style>
    </a>
    
  )
}
function Instances(props) {
  
  return (
    <Layout>
      <div>
        <h1>Other instances</h1>
        <HelpBlock title="Exhaustive list of interoperable instances" text={`You'll find other instances which might be interconnected. The Followed status implies that data on this instance is retrieved on the homescreen.\n Note that you can't (yet) choose to follow / unfollow another instance.`} />
        {/* <pre>{JSON.stringify(instances ,null,2)}</pre> */}
        {instances.map(instance => <InstanceRow {...{instance}}/>)}
      </div>
    </Layout>
  )
}

export default Instances
