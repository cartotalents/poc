import Layout from '../../components/Layout'
import Router, { useRouter } from 'next/router'
import { withApollo } from '../../apollo/client'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'
import HelpBlock from '../../components/HelpBlock'
import { MD5 } from '../index'

const UserQuery = gql`
  query UserQuery($userId: String!) {
    user(userId: $userId) {
      id
      email
      name
      geoLat
      geoLon
    }
  }
`

const DeleteMutation = gql`
  mutation DeleteMutation($userId: String!) {
    deleteUser(userId: $userId) {
      id
    }
  }
`

function User() {
  const userId = useRouter().query.id
  const { loading, error, data } = useQuery(UserQuery, {
    variables: { userId },
  })

  const [deleteUser] = useMutation(DeleteMutation)

  if (loading) {
    console.log('loading')
    return <div>Loading ...</div>
  }
  if (error) {
    console.log('error')
    return <div>Error: {error.message}</div>
  }

  console.log(`response`, data)
  //Unwrap
  const { id, name, email, geoLat, geoLon } = data.user;

  return (
    <Layout>
      <div>
        <div className="user">
          <div>
            <img className="local-border avatar" src={`//www.gravatar.com/avatar/${MD5(email)}?d=robohash&s=200`} />
          </div>
          <div>
            <h1>{name}</h1>
            <h2>ID: {id}</h2>
            <h2>{email}</h2>
            <p>Longitude : {geoLon} / Latitude : {geoLat}</p>
          </div>
          <button
            className="button"
            onClick={async e => {
              await deleteUser({
                variables: {
                  userId,
                },
              })
              Router.push('/')
            }}>
            Delete
          </button>
        </div>
        <HelpBlock title="About user" text={`If you delete this user, it will disappear of other instances which are following this instance.\n\nAvatar are automatically generated by courtesy of Gravatar (https://www.gravatar.com).\n\nNote that it is intended that you cannot edit these informations.`} />
      </div>
      <style jsx>{`
        .page {
          background: white;
          padding: 2rem;
        }
        .user {
          text-align:center;
          margin:20px;
        }
        .actions {
          margin-top: 2rem;
        }
        small {
          color: gray;
        }

        button {
          background: #bb0000;
        }

        button + button {
          margin-left: 1rem;
        }
        .avatar {
          background: white;
          border-radius: 200px;
        }
      `}</style>
    </Layout>
  )
}

export default withApollo(User)
