# Fullstack Example with Next.js (GraphQL API)

This example shows how to implement a **fullstack app in TypeScript with [Next.js](https://nextjs.org/)** using [React](https://reactjs.org/), [Apollo Client](https://www.apollographql.com/docs/react/) (frontend), [GraphQL Nexus](https://nexus.js.org/) and [Prisma Client](https://github.com/prisma/prisma2/blob/master/docs/prisma-client-js/api.md) (backend). It uses a SQLite database file with some initial dummy data which you can find at [`./prisma/dev.db`](./prisma/dev.db).

## How to use

### 1. Download example & install dependencies

Clone this repository:

```
git clone git@github.com:prisma/prisma-examples.git --depth=1
```

Install npm dependencies:

```
cd prisma-examples/typescript/graphql-nextjs
npm install
```

Note that this also generates Prisma Client JS into `node_modules/@prisma/client` via a `postinstall` hook of the `@prisma/client` package from your `package.json`.

### 2. Start the app

#### Traditional way

```
npm run dev
```

The app is now running, navigate to [`http://localhost:3000/`](http://localhost:3000/) in your browser to explore its UI.

#### Create 3 instances at the same time:

**ActivityPub works only on HTTPS, you have to configure a self-signed certificate with mkcert**

  1 - Install [mkcert](https://github.com/FiloSottile/mkcert)

  2 - Create certificate inside `docker-compose/certs` folder

```bash
cd docker-compose
mkcert -install
mkcert -cert-file dev/certs/local-cert.pem -key-file dev/certs/local-key.pem "poc.local" "*.poc.local"
```

3 - Launch Docker containers:

```bash
cd docker-compose

# dev
docker-compose -f docker-compose-dev.yml up --build

#prod
docker-compose -f docker-compose-prod.yml up --build
```

4 - Add urls on `etc/hosts` file

```bash
sudo nano /etc/hosts
```

```
##### POC #####
127.0.0.1 a.poc.local
127.0.0.1 b.poc.local
127.0.0.1 c.poc.local
```

##### Acces to instances

- Instance A: `https://a.poc.local` || `http://localhost:3010`
- Instance B: `https://b.poc.local` || `http://localhost:3020`
- Instance C: `https://c.poc.local` || `http://localhost:3030`

#### Kill instances

```bash
cd docker-compose

# dev
docker-compose -f docker-compose-dev.yml down

#prod
docker-compose -f docker-compose-prod.yml down
```

## Documentation

- Read the holistic, step-by-step [Prisma Framework tutorial](https://github.com/prisma/prisma2/blob/master/docs/tutorial.md)
- Check out the [Prisma Framework docs](https://github.com/prisma/prisma2) (e.g. for [data modeling](https://github.com/prisma/prisma2/blob/master/docs/data-modeling.md), [relations](https://github.com/prisma/prisma2/blob/master/docs/relations.md) or the [Prisma Client API](https://github.com/prisma/prisma2/tree/master/docs/prisma-client-js/api.md))
- Share your feedback in the [`prisma2-preview`](https://prisma.slack.com/messages/CKQTGR6T0/) channel on the [Prisma Slack](https://slack.prisma.io/)
- Create issues and ask questions on [GitHub](https://github.com/prisma/prisma2/)
- Track Prisma 2's progress on [`isprisma2ready.com`](https://isprisma2ready.com)
