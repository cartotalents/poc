const express = require('express')
const next = require('next')
const bodyParser = require('body-parser')

const dev = process.env.NODE_ENV !== 'production'

//Check if all needed envvars are defined
const mandatory_envvars = ["INSTANCE_ID", "INSTANCE_URL", "MAPBOX_TOKEN"],
  defined_envvars = Object.keys(process.env),
  missing_envvars = mandatory_envvars.filter(envvar => !defined_envvars.includes(envvar));
//Stop server now if missing
if (missing_envvars.length) {
  throw `Missing mandatory environment variable : ${missing_envvars.join(',')}`
}

//https://nextjs.org/docs/advanced-features/custom-server
const app = next({
  dev,
  env: {
    MAPBOX_TOKEN: process.env.MAPBOX_TOKEN,
  }
})

const handle = app.getRequestHandler()

const api = require('./api/index.ts')

app.prepare().then(() => {
  const server = express()
  process.env.PORT = process.env.PORT || 3000;

  //Service des fichiers statiques de leaflet via express
  server.use('/favicon', express.static('favicon'));
  //Body parser middleware, included in next.js handler but not express
  server.use(bodyParser.json());
  server.use('/api', api);
  
  server.get('/_next*|*', (req, res) => {
    return handle(req, res)
  });

  server.listen(process.env.PORT, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${process.env.PORT}`)
  })
})
.catch(ex => {
  console.error(ex.stack)
  process.exit(1)
})
