const instances = require('./instances.json');
const SELF_INSTANCE_ID = process.env.INSTANCE_ID;

export interface Instance {
  id: String
  name: String
  url: String
  isFollowed: Boolean
}

/**
 * Discovery module, which uses a static file listing the instances and
 * describing the relationships between the instances for now (instances.json)
 */
class DiscoveryAPI {
  /**
   * Retrieve all instances (including self)
   * @param id ex: a.poc.local
   */
  getInstances = () : [Instance] => {
    return instances;
  }

  /**
   * Retrieve all instances followed (or not, depending on param), excluding self
   * @param isFollowed defaults to true
   */
  getInstanceFollowed = (isFollowed: Boolean = true): [Instance]  => {
    return instances.filter(instance => instance.isFollowed == isFollowed && instance.id != SELF_INSTANCE_ID);
  }

  /**
   * Retrieve one instance with its id
   * @param id ex: a.poc.local
   */
  getInstance = (id: String): Instance => {
    return instances.filter(instance => instance.id == id);
  }

}

module.exports = {
  api: new DiscoveryAPI()
} 