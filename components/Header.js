import Link from 'next/link'

function isActive(pathname) {
  return (
    typeof document !== 'undefined' && document.location.pathname === pathname
  )
}

const Header = () => (
  <nav>
    <div className="left">
      <Link href="/">
        <a className="title menu-link" data-active={isActive('/')}>
          <h1>
            {process.env.INSTANCE_ID}
          </h1>
        </a>
      </Link>
      <Link href="/">
        <a className="menu-link" data-active={isActive('/')}>
          Map
        </a>
      </Link>
      <Link href="/instances">
        <a className="menu-link" data-active={isActive('/instances')}>
          Other instances
        </a>
      </Link>
    </div>
    <div className="right">
      <Link href="/signup">
        <a className="button" data-active={isActive('/signup')}>Create user</a>
      </Link>
    </div>
    <style jsx>{`
      nav {
        display: flex;
        padding: 1.2rem 2rem;
        align-items: center;
        background: white;
        border-bottom: 3px solid #3c72b9;
        box-shadow: 0px 1px 1px #aaa;
      }

      .bold {
        font-weight: bold;
      }
      .title h1 {
        margin:0;
        margin-right: 20px;
        color: black;
        font-weight: 300;
      }

      a.menu-link {
        text-decoration: none;
        color: gray;
        display: inline-block;
      }

      .left a[data-active='true'] {
        color: #000;
        font-weight: 600;
      }

      a + a {
        margin-left: 1rem;
      }

      .right {
        margin-left: auto;
      }

      .right a {
      }
    `}</style>
  </nav>
)

export default Header
