import ReactDOMServer from 'react-dom/server';
import Link from 'next/link'
import { useEffect, useRef } from 'react';
import mapboxgl from "mapbox-gl";

//Using next.js scoped css for injection on newly created marker, which isn't scoped by default
//https://nextjs.org/docs/basic-features/built-in-css-support#adding-component-level-css
import moduleStyles from './Map.module.css'
import { MD5 } from '../../pages/index'

mapboxgl.accessToken = process.env.MAPBOX_TOKEN;

interface MarkerProps {
  id: any,
  geoLon: number, 
  geoLat: number, 
  name?: string,
  email?: string,
  /**
   * Possible instance id, where the data comes from
   */
  instance_id?: string
}
interface MapProps {
  /**
   * Array of markers to display at map render.
   */
  markers?: [MarkerProps]

  /**
   * Optional style override
   */
  style?: {}
}

const defaultMapCenter = {
  lng: -1.569431,
  lat: 47.204438,
  zoom: 4
}
const getInitials = (name: string) => {
  return name.replace(/[^A-Z]/g, "")
}
const generateMarkerMarkup = (marker: MarkerProps) => {
  return ReactDOMServer.renderToStaticMarkup(
    <img className={marker.instance_id ? 'remote-border' : 'local-border'} src={`//www.gravatar.com/avatar/${MD5(marker.email)}?d=robohash&s=30`} />
  )
}
const generatePopupMarkup = (user: MarkerProps) => {
  return ReactDOMServer.renderToStaticMarkup(<div>
      <h2>{user.name}</h2>
      <p>{user.email}</p>
      {typeof user.instance_id == "string" ? (
        <div>
          <p>Remote user from {user.instance_id}</p>
          <a className={`button ${user.instance_id ? 'remote-background' : 'local-background'}`} href={`//${user.instance_id}/user/${user.id.replace(`${user.instance_id}.`, '')}`}>See on remote site</a>
        </div>
      ) : (
          <div>
            <p>Local user</p>
            <Link href="/user/[id]" as={`/user/${user.id}`}>
              <a className={`button ${user.instance_id ? 'remote-background' : 'local-background'}`}>See more</a>
            </Link>
          </div>
        )}
    </div>
  )
}
const generatePopup = (marker: MarkerProps) => {
  return new mapboxgl.Popup({ offset: 25 }).setHTML(generatePopupMarkup(marker))
}
const Map = (props: MapProps) => {
  const { markers, style = {} } = props;
  const mapContainerHTML = useRef(null);
  const map = useRef(null);

  useEffect(() => {
    map.current = new mapboxgl.Map({
      container: mapContainerHTML.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [defaultMapCenter.lng, defaultMapCenter.lat],
      zoom: defaultMapCenter.zoom
    });
  }, [])

  useEffect(() => {
    if (map && markers.length){
      markers.forEach(marker => {

        // create a HTML element for each feature
        var el = document.createElement('div');
        el.className = moduleStyles.marker;
        el.innerHTML = generateMarkerMarkup(marker);

        new mapboxgl.Marker(el)
          .setLngLat([marker.geoLon, marker.geoLat])
          .setPopup(generatePopup(marker))
          .addTo(map.current);
      })
    }
  }, [markers])
  return (
    <div className="mapWrapper" style={style}>
      <div ref={el => mapContainerHTML.current = el} className='mapContainer' />
      <style jsx>{`
        .mapWrapper {
          position: relative;
        }
        .mapContainer {
          position: absolute;
          top: 0;
          right: 0;
          left: 0;
          bottom: 0;
        }
      `}</style>
    </div>
  )
}

export default Map
