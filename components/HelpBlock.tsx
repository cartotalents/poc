interface HelpBlockProps {
  title?: String
  text?: String
}
const HelpBlock = ({title, text}: HelpBlockProps) => (
  <div className="help-block">
    {!!title && <h2>{title}</h2>}
    {!!text && <p>{text}</p>}
    <style jsx>{`
      .help-block {
        margin: 1.5rem 0;
        padding: 0.2rem 1.2rem 0.5rem;
        border-left: 2px solid #3c72b9;
        background: white;
 
      }
      h2 {
        font-size: 18px;
      }
      p {
        font-size: 14px;
        white-space: pre-line;
        color: gray;
      }
    `}</style>
  </div>
)

export default HelpBlock
