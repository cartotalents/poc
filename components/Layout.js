import Header from './Header'

const Layout = props => (
  <div>
    <Header />
    <div className="layout">{props.children}</div>
    <style jsx global>{`
      html {
        box-sizing: border-box;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }

      body {
        margin: 0;
        padding: 0;
        font-size: 16px;
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
          Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji',
          'Segoe UI Symbol';
        background: rgba(0, 0, 0, 0.05);
      }

      input,
      textarea {
        font-size: 16px;
      }

      button {
        cursor: pointer;
      }
      .button {
        border: none;
        padding: 0.5rem 1rem;
        font-size: 14px;
        border-radius: 3px;
        color: white;
        background: #3c72b9;
      }
      a.button {
        color: white;
        text-decoration: none;
      }
      h1, h2, h3 {
        font-weight: 300;
      }

      .local-background {
        background: #3c72b9;
      }
      .remote-background {
        background: #e89616;
      }
      .local-color {
        color: #3c72b9;
      }
      .remote-color {
        color: #e89616;
      }
      .local-border {
        border: 2px solid #3c72b9;
      }
      .remote-border {
        border: 2px solid #e89616;
      }
    `}</style>
    <style jsx>{`
      .layout {
        padding: 0 2rem;
      }
    `}</style>
  </div>
)

export default Layout
