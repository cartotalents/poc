module.exports = {
  env: {
    MAPBOX_TOKEN: process.env.MAPBOX_TOKEN,
    INSTANCE_ID: process.env.INSTANCE_ID,
    INSTANCE_URL: process.env.INSTANCE_URL,
  },
}